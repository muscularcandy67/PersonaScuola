package com.muscularcandy67;

public class Persona {

    private String nome;
    private String cognome;
    private String titolo;
    private String professione;

    public Persona(String nome, String cognome, String titolo, String professione) {
        setNome(nome);
        setCognome(cognome);
        setTitolo(titolo);
        setProfessione(professione);
    }


    public Persona(String nome, String cognome) {
        setNome(nome);
        setCognome(cognome);
        titolo="";
        professione="disoccupato";

    }

    public String getNome() {
        return nome;
    }

    public boolean setNome(String nome) {
        if(nome!=null){
            if(nome.matches("[a-zA-Z]+")) {
                this.nome = nome;
                return true;
            }
            else return false;
        }
        else return false;
    }

    public String getCognome() {
        return cognome;
    }

    public boolean setCognome(String cognome) {
        if(cognome!=null){
            if(cognome.matches("[a-zA-Z]+")) {
                this.cognome = cognome;
                return true;
            }
            else return false;
        }
        else return false;
    }

    public String getTitolo() {
        return titolo;
    }

    public boolean setTitolo(String titolo) {
        if(titolo!=null){
            if(titolo.matches("[a-zA-Z]+")) {
                this.titolo = titolo;
                return true;
            }
            else return false;
        }
        else return false;
    }

    public String getProfessione() {
        return professione;
    }

    public boolean setProfessione(String professione) {
        if(professione!=null){
            if(professione.matches("[a-zA-Z]+")) {
                this.professione = professione;
                return true;
            }
            else return false;
        }
        else return false;
    }

    public boolean impostaTitolo(String titolo){
        return setTitolo(titolo);
    }

    public boolean impostaProfessione(String professione){
        return setProfessione(professione);
    }

    public String ottieniInfoPersona(){
        StringBuilder s = new StringBuilder();
        if(titolo!=null) s.append(titolo);
        s.append(nome).append(" ").append(cognome).append(", ").append(" professione").append(professione);
        return s.toString();
    }
}
